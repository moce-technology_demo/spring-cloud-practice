package org.nr.tour.common.service;

import org.nr.tour.domain.Hotel;

/**
 * @author chenhaiyang <690732060@qq.com>
 */
public interface HotelServiceDefinition extends AbstractServiceDefinition<Hotel, String> {
}
