package org.nr.tour.constant;

/**
 * @author chenhaiyang <690732060@qq.com>
 */
public interface ServiceConstants {
    String VERIFY_CODE_SERVICE = "verify-code-service";
    String SMS_SERVICE = "sms-service";
    String HOTEL_SERVICE = "hotel-service";
    String VISA_SERVICE = "visa-service";
    String DICT_SERVICE = "dict-service";
    String MEMBER_SERVICE = "member-service";
    String PATH_SERVICE = "service";
    String PATH_SERVICE_CATEGORY = "service/category";
    String PATH_HOTEL_SUPPORT_SERVICE = "hotel/support/service";
    String LINE_SERVICE = "line-service";
    String SCENERY_SERVICE = "scenery-service";
}
