package org.nr.tour.common.service;

import org.nr.tour.domain.Line;

/**
 * @author chenhaiyang <690732060@qq.com>
 */
public interface LineServiceDefinition extends AbstractServiceDefinition<Line,String> {
}
